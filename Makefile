.PHONY: clean purge lib install

CCX = g++
CCXFLAGS = -std=c++20 -Wall -Wextra -Werror -pedantic -O3 -fPIC
CPPFILES = $(wildcard src/*.cpp)
HFILES = $(wildcard src/*.h)
OFILES = $(patsubst %.cpp,%.o, $(CPPFILES))

INSTALL_PATH ?= /usr/local
AR = ar
ARFLAGS = rcs

morse: $(OFILES) $(HFILES)
	@ $(CCX) $(OFILES) -o morse $(CCXFLAGS)

lib: $(OFILES) morse
	@ $(CCX) -shared -o libmorse.so src/morse.o
	@ $(AR) $(ARFLAGS) libmorse-static.a src/morse.o

install: lib morse
	@ install -d $(INSTALL_PATH)/lib ;
	@ install -m 644 ./libmorse* $(INSTALL_PATH)/lib ;
	@ echo " libmorse.so => $(INSTALL_PATH)/lib"
	@ echo " libmorse-static.a => $(INSTALL_PATH)/lib"
	@ install -d $(INSTALL_PATH)/include ;
	@ install -m 644 src/morse.h $(INSTALL_PATH)/include/morse.h
	@ echo " morse.h => $(INSTALL_PATH)/include"
	@ install -d $(INSTALL_PATH)/bin ;
	@ install -m 755 ./morse $(INSTALL_PATH)/bin ;
	@ install -d $(INSTALL_PATH)/share/man/man1 ;
	@ install -m 644 morse.1 $(INSTALL_PATH)/share/man/man1 ;
	@ echo " morse.1 => $(INSTALL_PATH)/share/man/man1"

uninstall:
	@ $(foreach F, \
			$(INSTALL_PATH)/bin/morse \
			$(INSTALL_PATH)/lib/libmorse.so \
			$(INSTALL_PATH)/lib/libmorse-static.a \
			$(INSTALL_PATH)/include/morse.h \
			$(INSTALL_PATH)/share/man/man1/morse.1, \
		echo " RM $F" ; rm -f $F ; )

.cpp.o:
	@ echo " CC $^"
	@ $(CCX) $(CCXFLAGS) -o $@ -c $<

%/:
	@ mkdir $@

clean:
	@ find . -name '*.o' -delete
	@ rm -f ./morse
	@ rm -f ./libmorse*
