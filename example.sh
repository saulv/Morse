#!/bin/bash

# Example script, prints the alphabet in morse

if [ ! -f "./morse" ] ; then
	make > /dev/null
fi

alphabet=(a b c d e f g h i j k l m n o p q r s t u v w x y z)

for i in ${alphabet[@]} ;
do
	m="$(./morse $i)" ;
	echo "$i => $m";
done
