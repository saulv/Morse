/*
 * Implementation of the Morse class
 * Author: Saúl Valdelvira (2023)
*/

#include "morse.h"
#include <cassert>
using std::make_shared;

Morse::DecoderNode::DecoderNode() : DecoderNode('\0'){}

Morse::DecoderNode::DecoderNode(char c){
        info = c;
        left = NULL;
        right = NULL;
}

void Morse::add(string s, char value) {
        shared_ptr<DecoderNode> aux = decoder_root;
	for (size_t i = 0; i < s.length(); i++) {
		if (s[i] == DASH) {
			if(aux->right == NULL)
				aux->right = make_shared<DecoderNode>();
			aux = aux->right;
		}
		else if (s[i] == DOT) {
			if(aux->left == NULL)
				aux->left = make_shared<DecoderNode>();
			aux = aux->left;
		}
		else {
                        assert(0 && "Unreachable");
		}

	}
	aux->info = value;
	encoder[value] = s;
}

char Morse::_decode(string s){
        if (s == "/") {
		return ' ';
	}else if (s == "#") {
		return '#';
	}else if (s == "\t") {
		return '\t';
	}
	shared_ptr<DecoderNode> aux = decoder_root;
	for (size_t i = 0; i < s.length(); i++) {
		if (s[i] == DASH)
			aux = aux->right;
		else if (s[i] == DOT)
			aux = aux->left;
		if (aux == NULL)
			return '#';
	}
	if (letter_case == UPPERCASE)
		return std::toupper(aux->info);
	else
		return std::tolower(aux->info);
}

string Morse::_encode(char c){
        c = std::toupper(c);
	if(!encoder.contains(c))
		return "#";
	return encoder[c];
}

Morse::Morse() :  Morse(LOWERCASE) {}

Morse::Morse(enum letter_case _letter_case) {
	letter_case = _letter_case;

	// the decoder_root node does not contain anything, it is just a start point.
	decoder_root = std::make_shared<DecoderNode>(' ');
	add(".", 'E');
	add("-", 'T');
	add("..", 'I');
	add(".-", 'A');
	add("-.", 'N');
	add("--", 'M');
	add("...", 'S');
	add("..-", 'U');
	add(".-.", 'R');
	add(".--", 'W');
	add("-..", 'D');
	add("-.-", 'K');
	add("--.", 'G');
	add("---", 'O');
	add("....", 'H');
	add("...-", 'V');
	add("..-.", 'F');
	add(".-..", 'L');
	add(".--.", 'P');
	add(".---", 'J');
	add("-...", 'B');
	add("-..-", 'X');
	add("-.-.", 'C');
	add("-.--", 'Y');
	add("--..", 'Z');
	add("--..--", ',');
	add(".-.-.-", '.');
	add("..--..", '?');
	add("--.-", 'Q');
	add(".....", '5');
	add("....-", '4');
	add("...--", '3');
	add("..---", '2');
	add(".-.-.", '+');
	add(".----", '1');
	add("-....", '6');
	add("-...-", '=');
	add("-..-.", '/');
	add("--...", '7');
	add("---..", '8');
	add("----.", '9');
	add("-----", '0');

	//add these extra characters to the encoder
	encoder[' '] = "/";
	encoder['\n'] = "\n";
	encoder['\t'] = "\t";
}

string Morse::decode(string msg){
        string aux = "";
	string result = "";
	for (size_t i = 0; i < msg.length(); i++) {
		if (msg[i] == ' ') {
			result += _decode(aux);
			aux = "";
		}
		else if (msg[i] == '\n'){
			if (!aux.empty()){
				result += _decode(aux);
			}
			result += "\n";
			aux = "";
		}else {
			aux += msg[i];
		}
	}
	if (!aux.empty()){
		result += _decode(aux); // Decode the remaining
	}
	return result;
}

string Morse::encode(string msg){
        string result = "";
	for (size_t i = 0; i < msg.length(); i++) {
		result += _encode(msg[i]);
		if(msg[i] != '\n'){
			result += " ";
		}
	}
	return result;
}

bool Morse::is_morse_char(char c){
	return MORSE_CHARACTERS.find(c) != string::npos;
}

void Morse::set_letter_case(enum letter_case _letter_case){
	letter_case = _letter_case;
}

