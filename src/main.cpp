/*
 * Main program to process morse text.
 * Author: Saúl Valdelvira (2023)
*/

#include "morse.h"
#include <iostream>
using std::cout, std::cin, std::cerr, std::endl;
#include <cstring>

Morse morse;

#define ENCODE 1
#define DECODE 0

void help();
int detect(string s);

int main(int argc, char *argv[]) {
	int mode = -1;
	string text = "";

	for (int i = 1; i < argc; i++){
		// If the first character is a '-' and the next one is NOT a morse character, this means the '-' is an argument, not a morse coded word.
		if (argv[i][0] == '-' && argv[i][1] != '\0' && !morse.is_morse_char(argv[i][1])){
			for (int j = 1; argv[i][j] != '\0'; j++){
				switch (argv[i][j]){
				case 'e':
				case 'E':
					mode = ENCODE;
					break;
				case 'd':
				case 'D':
					mode = DECODE;
					break;
				case 'u':
				case 'U':
					morse.set_letter_case(Morse::UPPERCASE);
					break;
				case 'l':
				case 'L':
					morse.set_letter_case(Morse::LOWERCASE);
					break;
				case 'h':
				case 'H':
					help();
					break;
				default:
					cerr << "Invalid argument: " << argv[i][j] <<
					        "\nUse --help or -h to see a list of valid arguments" << endl;
					exit(1);
				}
			}
		}else if (argv[i][0] == '-' && argv[i][1] == '-' && argv[i][2] != '\0' && !morse.is_morse_char(argv[i][2])){
			if (strcmp("decode", &argv[i][2]) == 0){
				mode = DECODE;
			}
			else if (strcmp("encode", &argv[i][2]) == 0){
				mode = ENCODE;
			}
			else if (strcmp("lowercase", &argv[i][2]) == 0){
				morse.set_letter_case(Morse::LOWERCASE);
			}
			else if (strcmp("uppercase", &argv[i][2]) == 0){
				morse.set_letter_case(Morse::UPPERCASE);
			}
			else if (strcmp("help", &argv[i][2]) == 0){
				help();
			}
			else{
				cerr << "Invalid argument: " << &argv[i][2] <<
				        "\nUse --help or -h to see a list of valid arguments" << endl;
				exit(1);
			}

		}else { // The argument is the input
			int j = 0;
			while (argv[i][j] != '\0'){
				text.append(1, argv[i][j++]);
			}
			if (i < argc - 1){ // Add a space if it's not the last argument
				text.append(1, ' ');
			}
		}
	}

	// No input from command line, this means the input is taken from the standard input
	if (text.empty()){
		string buf;
		while (getline(cin, buf)) {
			text += buf;
			if (!cin.eof()){
				text += '\n';
			}
		}
	} else {
		text += '\n';
	}

	if (mode == -1){
		mode = detect(text);
	}

	if (mode == ENCODE){
		cout << morse.encode(text);
	} else {
		cout << morse.decode(text);
	}

	return 0;
}

/**
 * Prints a help menu on console
*/
void help(){
	cout <<
	" A morse code translator.\n" <<
	" Usage: morse [...] [-edulh] [...] \n" <<
	" Parameters:\n" <<
	"   -e, --encode: Takes the input as a text to be encoded.\n" <<
	"   -d, --decode: Takes the input as a morse to be decoded.\n" <<
	"   -u, --uppercase: The output text will be uppercase.\n" <<
	"   -l, --lowercase: The output text will be lowercase.\n" <<
	"   -h, --help: Display a help manual.\n" <<
	"   [...]: The input can be given as a command line parameter (e.g. morse -e \"text to encode\")\n" <<
	" Note: If no parameter is given, the program will try to guess which codification the input has.\n" <<
	" Source code: https://git.saulv.es/Morse\n";
	exit(0);
}

/**
 * Detects the mode in wich the text should be processed.
 * If there is at least one non-morse character (like 'a' or a comma for example), returns ENCODE.
 * Else it assumes that the input is Morse code.
*/
int detect(string s){
	for (size_t i = 0; i < s.length(); i++){
		if (!morse.is_morse_char(s[i])){
			return ENCODE;
		}
	}
	return DECODE;
}
