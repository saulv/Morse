/*
 * Declaration of the Morse class
 * Author: Saúl Valdelvira (2023)
*/

#pragma once
#ifndef MORSE_H
#define MORSE_H
#include <map>
using std::map;
#include <string>
#include <memory>
using std::string;
using std::shared_ptr;

/// Morse class. It can encode or decode text to and from morse.
class Morse {

private:
	// Node of the BSTree used to decode morse text.
	typedef struct DecoderNode
	{
		char info;
		shared_ptr<struct DecoderNode> left;
		shared_ptr<struct DecoderNode> right;
		DecoderNode();
		DecoderNode(char c);
	} DecoderNode;

	shared_ptr<DecoderNode> decoder_root;

	map<char, string> encoder;

	const char DASH = '-';
	const char DOT = '.';

	// String containing all the characters used in morse text
	const string MORSE_CHARACTERS = ".-#/ \n\t";

	const string SPECIAL_CHARACTERS = "\n\t ";

	/**
	 * Decodes a morse character
	*/
	char _decode(string s);

	/**
	 * Encodes a character
	*/
	string _encode(char c);

public:
	/**
	 * Adds a charcter to both the decoder and the encoder.
	 * @param s the morse representation of the character
	 * @param value the character to add
	*/
	void add(string s, char value);

	enum letter_case{
		UPPERCASE, LOWERCASE
	} letter_case;

	void set_letter_case(enum letter_case _letter_case);

	/**
	 * @return true if the given character is a character used in morse text (dash, dot, '#', etc.)
	*/
	bool is_morse_char(char c);

	Morse();
	Morse(enum letter_case _letter_case);

	string decode(string msg);

	string encode(string msg);
};

#endif
